-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2015 at 11:41 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dash`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(3) NOT NULL,
  `admin_full_name` varchar(50) NOT NULL,
  `admin_email_address` varchar(100) NOT NULL,
  `admin_password` varchar(32) NOT NULL,
  `access_label` tinyint(1) NOT NULL,
  `profile_photo` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_full_name`, `admin_email_address`, `admin_password`, `access_label`, `profile_photo`) VALUES
(1, 'deep kumar das', '4deepkumar@gmail.com', 'b642cdcff9bfe56194063ef1764b652e', 1, 'assets/profile_image/11707498_1444056042569265_7144065580115963100_n2.jpg'),
(2, 'Rhitwick Pramanik', 'rhitwick@gmail.com', '2138cb5b0302e84382dd9b3677576b24', 0, 'assets/profile_image/Rhitwick_Pramanik.jpg'),
(3, 'ayon manna', 'ayonmanna@gmail.com', '2138cb5b0302e84382dd9b3677576b24', 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `ajax`
--

CREATE TABLE IF NOT EXISTS `ajax` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ajax`
--

INSERT INTO `ajax` (`id`, `name`, `email`) VALUES
(1, 'deep', 'deep@gmail.com'),
(2, 'dash', 'dash@gmail.com'),
(3, 'make', 'make@gmail.com'),
(4, 'park', 'take@gmail.com'),
(5, 'take', 'take@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `blog_id` int(5) NOT NULL,
  `category_id` int(3) NOT NULL,
  `blog_title` varchar(256) CHARACTER SET utf8 NOT NULL,
  `blog_short_description` text CHARACTER SET utf8 NOT NULL,
  `blog_long_description` text CHARACTER SET utf8 NOT NULL,
  `blog_image` varchar(122) NOT NULL,
  `author_name` varchar(50) NOT NULL,
  `publication_status` tinyint(1) NOT NULL,
  `hit_count` int(11) NOT NULL DEFAULT '0',
  `stikey_post` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `category_id`, `blog_title`, `blog_short_description`, `blog_long_description`, `blog_image`, `author_name`, `publication_status`, `hit_count`, `stikey_post`) VALUES
(4, 9, 'জল্পনার অবসান, নতুন দল, জাতীয়তাবাদী তৃণমূল কংগ্রেস গড়লেন মুকুল রায়', '<p><span [removed] rgb(70, 70, 70); font-family: shree_ban_otf_0592; font-size: 22px; line-height: 25px;">জানাই ছিল নতুন দল গঠনের পথেই হাঁটছেন মুকুল। কিন্তু কবে কীভাবে দল গঠন করবেন মুকুল সে বিষয়ে স্পষ্ট করে কিছুই জানা যাচ্ছিল না। আজ সব জল্পনার ইতি ঘটিয়ে নয়া দল গঠন করে বিচ্ছেদে পাকাপাকি শীলমোহর লাগালেন এই সাংসদ। </span><br></p>', '<div class="article-image" [removed]="color: rgb(51, 51, 51); font-family: ''Roboto Condensed'', sans-serif; line-height: 18.5714px;"><div class="field-items"><div class="field-item even"><p [removed]="font-family: shree_ban_otf_0592; color: rgb(70, 70, 70); line-height: 25px; font-size: 22px; padding-bottom: 6px;">ফাটল জন্মেছিল বহু আগেই। সারদা কাণ্ডে মুকুল রায়কে জিজ্ঞাসাবাদের পর থেকেই সেই ফাটল স্পষ্ট হয়। একদা মমতার বিসস্ততম সঙ্গী মুকুল রায়ের বিরুদ্ধে বহুবার সর্বসমক্ষে মুখ খোলেন স্বয়ং দল নেত্রী। </p><p [removed] shree_ban_otf_0592; color: rgb(70, 70, 70); line-height: 25px; font-size: 22px; padding-bottom: 6px;">জানাই ছিল নতুন দল গঠনের পথেই হাঁটছেন মুকুল। কিন্তু কবে কীভাবে দল গঠন করবেন মুকুল সে বিষয়ে স্পষ্ট করে কিছুই জানা যাচ্ছিল না। আজ সব জল্পনার ইতি ঘটিয়ে নয়া দল গঠন করে বিচ্ছেদে পাকাপাকি শীলমোহর লাগালেন এই সাংসদ। </p></div></div></div>', '', 'deep kumar das', 1, 18, 0),
(5, 5, 'Cop suspended after he smashes typewriter of an elderly man in Lucknow', '<p class="body" [removed]="outline: 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">Krishna Kumar (65) had been sitting on the pavement outside the General Post Office (GPO) working as the typist for the last 35 years, a senior officer said.</p><p class="body" [removed] 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">On pretext of “security movement”, Incharge of the Secretariat Police outpost Sub Inspector Pradeep Kumar asked Kumar to vacate the place yesterday and even crushed the latter’s typewriter under his boots.</p>', '<p class="body" [removed]="outline: 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">As the pictures of the incident went viral on the social media, the CM asked senior officers to suspend the errant cop and provide a new typewriter to the victim.</p><p class="body" [removed]="outline: 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">District Magistrate Rajshekhar and SSP Pandey later visited Kumar’s house and handed over a typewriter to him.</p><p class="body" [removed] 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">The CM has asked the senior officers to train policemen in improving their behaviour while dealing with the public, the officer said.</p>', '', 'deep kumar das', 1, 30, 1),
(6, 5, 'Cop suspended after he smashes typewriter of an elderly man in Lucknow', '<p class="body" [removed] 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">The pleas of the elderly man went unheard and the SI continued with the act till the typewriter was completely smashed.</p>', '<p class="body" [removed]="outline: 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">As the pictures of the incident went viral on the social media, the CM asked senior officers to suspend the errant cop and provide a new typewriter to the victim.</p><p class="body" [removed]="outline: 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">District Magistrate Rajshekhar and SSP Pandey later visited Kumar’s house and handed over a typewriter to him.</p><p class="body" [removed] 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">The CM has asked the senior officers to train policemen in improving their behaviour while dealing with the public, the officer said.</p>', '', 'deep kumar das', 1, 32, 0),
(7, 6, 'Cop suspended after he smashes typewriter', '<p class="body" [removed] 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">The pleas of the elderly man went unheard and the SI continued with the act till the typewriter was completely smashed.</p>', '<p class="body" [removed]="outline: 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">As the pictures of the incident went viral on the social media, the CM asked senior officers to suspend the errant cop and provide a new typewriter to the victim.</p><p class="body" [removed]="outline: 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">District Magistrate Rajshekhar and SSP Pandey later visited Kumar’s house and handed over a typewriter to him.</p><p class="body" [removed] 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">The CM has asked the senior officers to train policemen in improving their behaviour while dealing with the public, the officer said.</p>', '', 'deep kumar das', 1, 3, 0),
(8, 5, 'Cop suspended after he smashes typewriter', '<p class="body" [removed] 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">The pleas of the elderly man went unheard and the SI continued with the act till the typewriter was completely smashed.</p>', '<p class="body" [removed]="outline: 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">As the pictures of the incident went viral on the social media, the CM asked senior officers to suspend the errant cop and provide a new typewriter to the victim.</p><p class="body" [removed]="outline: 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">District Magistrate Rajshekhar and SSP Pandey later visited Kumar’s house and handed over a typewriter to him.</p><p class="body" [removed] 0px; margin-bottom: 20px; color: rgb(59, 58, 57); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 14px; line-height: 18px;">The CM has asked the senior officers to train policemen in improving their behaviour while dealing with the public, the officer said.</p>', '', 'deep kumar das', 1, 2, 0),
(9, 4, 'চলে গেলেন ডালমিয়া', '<p><span [removed] rgb(34, 34, 34); font-family: SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">ডালমিয়ার মৃত্যুতে বিসিসিআইয়ের সচিব অনুরাগ ঠাকুর টুইট করেছেন, ‘ভারতীয় ক্রীড়াজগতের মহানতম প্রশাসক চলে গেলেন। একটি যুগের সমাপ্তি।’</span><br></p>', '<p [removed]="padding: 0px; margin: 0px 10px 10px 0px; outline: 0px; text-align: inherit; display: inline-block; float: left; position: relative; overflow: hidden; max-width: 100%; width: 300px;"><img itemprop="image" width="300" src="http://paimages.prothom-alo.com/contents/cache/images/300x0x1/uploads/media/2015/09/20/ff781a33215e9094d713b4c388c2dbc9-e-mia.jpg" alt="ডালমিয়া" [removed]="padding: 0px; margin: 0px; outline: 0px;">তৃতীয়বারের মতো ভারতীয় ক্রিকেট কন্ট্রোল বোর্ডের সভাপতি হয়েছিলেন ডালমিয়া। আইসিসির সাবেক এই সভাপতি ছিলেন বাংলাদেশ ক্রিকেটের অকৃত্রিম বন্ধু। <br [removed]="padding: 0px; margin: 0px; outline: 0px;">ম্যাচ পাতানো কেলেঙ্কারির পর শ্রীনিবাসনের জায়গায় তৃতীয় দফায় ভারতীয় ক্রিকেট বোর্ডের সভাপতি হয়েছিলেন এ বছরের মার্চে। <br [removed]="padding: 0px; margin: 0px; outline: 0px;">১৯৮৩ সালে, অর্থাৎ যে বছরে ভারত বিশ্বকাপ জেতে ওই বছরই ভারতীয় ক্রিকেট বোর্ডের কোষাধ্যক্ষ হন ডালমিয়া। তিনি ১৯৮৭ (ভারত-পাকিস্তান যৌথ আয়োজক) এবং ১৯৯৬ সালে (ভারত, পাকিস্তান ও শ্রীলঙ্কা যৌথ আয়োজক) ভারতে বিশ্বকাপ আয়োজনে সহযোগিতা করেছেন। ভারতীয় বোর্ডের সভাপতি পদে থাকা অবস্থায় ১৯৯৭ সালে আইসিসির সভাপতি নির্বাচিত হন। ক্রিকেটের বিশ্বায়নের মন্ত্রও ছড়িয়ে দেন সে ওই সময়েই। ২০০০ সালে বাংলাদেশের টেস্ট স্ট্যাটাস পাওয়াতেও তাঁর বড় ভূমিকা ছিল। </p><p [removed] 0px; margin-bottom: 16px; outline: 0px; overflow: hidden; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">ডালমিয়ার মৃত্যুতে বিসিসিআইয়ের সচিব অনুরাগ ঠাকুর টুইট করেছেন, ‘ভারতীয় ক্রীড়াজগতের মহানতম প্রশাসক চলে গেলেন। একটি যুগের সমাপ্তি।’</p>', '', 'deep kumar das', 1, 5, 0),
(10, 6, 'ভারতীয় ক্রীড়াজগতের মহানতম প্রশাসক চলে গেলেন। একটি যুগের সমাপ্তি।’', '<p [removed] 0px; margin-bottom: 16px; outline: 0px; overflow: hidden; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">ভারতীয় ক্রীড়াজগতের মহানতম প্রশাসক চলে গেলেন। একটি যুগের সমাপ্তি।’<br></p>', '<p [removed]="padding: 0px; margin: 0px 10px 10px 0px; outline: 0px; text-align: inherit; display: inline-block; float: left; position: relative; overflow: hidden; max-width: 100%; width: 300px;"><img itemprop="image" width="300" src="http://paimages.prothom-alo.com/contents/cache/images/300x0x1/uploads/media/2015/09/20/ff781a33215e9094d713b4c388c2dbc9-e-mia.jpg" alt="ডালমিয়া" [removed]="padding: 0px; margin: 0px; outline: 0px;">তৃতীয়বারেরমতো ভারতীয় ক্রিকেট কন্ট্রোল বোর্ডের সভাপতি হয়েছিলেন ডালমিয়া। আইসিসির সাবেক এই সভাপতি ছিলেন বাংলাদেশ ক্রিকেটের অকৃত্রিম বন্ধু। <br [removed]="padding: 0px; margin: 0px; outline: 0px;">ম্যাচ পাতানো কেলেঙ্কারির পর শ্রীনিবাসনের জায়গায় তৃতীয় দফায় ভারতীয় ক্রিকেট বোর্ডের সভাপতি হয়েছিলেন এ বছরের মার্চে। <br [removed]="padding: 0px; margin: 0px; outline: 0px;">১৯৮৩ সালে, অর্থাৎ যে বছরে ভারত বিশ্বকাপ জেতে ওই বছরই ভারতীয় ক্রিকেট বোর্ডের কোষাধ্যক্ষ হন ডালমিয়া। তিনি ১৯৮৭ (ভারত-পাকিস্তান যৌথ আয়োজক) এবং ১৯৯৬ সালে (ভারত, পাকিস্তান ও শ্রীলঙ্কা যৌথ আয়োজক) ভারতে বিশ্বকাপ আয়োজনে সহযোগিতা করেছেন। ভারতীয় বোর্ডের সভাপতি পদে থাকা অবস্থায় ১৯৯৭ সালে আইসিসির সভাপতি নির্বাচিত হন। ক্রিকেটের বিশ্বায়নের মন্ত্রও ছড়িয়ে দেন সে ওই সময়েই। ২০০০ সালে বাংলাদেশের টেস্ট স্ট্যাটাস পাওয়াতেও তাঁর বড় ভূমিকা ছিল। </p><p [removed] 0px; margin-bottom: 16px; outline: 0px; overflow: hidden; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">ডালমিয়ার মৃত্যুতে বিসিসিআইয়ের সচিব অনুরাগ ঠাকুর টুইট করেছেন, ‘ভারতীয় ক্রীড়াজগতের মহানতম প্রশাসক চলে গেলেন। একটি যুগের সমাপ্তি।’</p>', '', 'deep kumar das', 1, 0, 0),
(11, 8, 'সৌদি সরকারকে দুষলেন বেঁচে যাওয়া হাজিরা', '<p><span [removed]="color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; font-size: 16px; line-height: 24px; padding: 0px; margin: 0px; outline: 0px;"><span [removed] rgb(34, 34, 34); font-family: SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">সৌদি আরবের মিনায় পবিত্র হজ পালনকালে গতকাল বৃহস্পতিবার পদদলিত হয়ে কমপক্ষে ৭১৭ জন হাজির মৃত্যু হয়। আহত হন আট শতাধিক হাজি।</span><br></p>', '<p [removed]="padding: 0px; margin: 0px; outline: 0px;">সৌদি আরবের মিনায় পবিত্র হজ পালনকালে গতকাল বৃহস্পতিবার পদদলিত হয়ে কমপক্ষে ৭১৭ জন হাজির মৃত্যু হয়। আহত হন আট শতাধিক হাজি।<br [removed]="padding: 0px; margin: 0px; outline: 0px;">এই হাজি আরও বলেন, ‘আমার সামনে অনেক মৃতদেহ পড়েছিল। অনেকে আহত হয়েছেন। অনেকের দমবন্ধ হয়ে আসছিল। পুলিশের সঙ্গে আমরাও অনেককে সরিয়েছি। সেখানে থাকা পুলিশদের কর্মকাণ্ড দেখে বোঝা গেছে তারা কতটা অনভিজ্ঞ। তারা মিনাকে ঘিরে থাকা রাস্তাঘাট ও এলাকাগুলো পর্যন্ত চেনে না।’</p><p [removed]="padding: 0px; margin: 0px; outline: 0px;">মিসরের ৩৯ বছরের মোহাম্মদ হাসান। তিনি আরেক প্রত্যক্ষদর্শী। তিনি বলেন, ‘এ পরিস্থিতি আবারও ঘটতে পারে। ঘটনার সময় এক জায়গায় দাঁড়িয়ে ছিল নিরাপত্তাকর্মীরা।’ তিনি অভিযোগ করেন, মিসরীয় হাজিদের মৃতদেহ শনাক্ত করার জন্য যখন তাঁকে ডাকা হয়, নিরাপত্তাকর্মীরা তখন তাঁর জাতীয়তা নিয়ে তাঁকে অপমান করেন।<br [removed]="padding: 0px; margin-bottom: 16px; outline: 0px; overflow: hidden; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">রাগ প্রকাশ করে এই মিসরীয় বলেন, হাজিদের নির্বিঘ্নে চলাচল নিশ্চিত করতে নিরাপত্তা বাহিনীর উচিত সড়কগুলো বিন্যস্ত করা।</p><p [removed]="padding: 0px; margin-bottom: 16px; outline: 0px; overflow: hidden; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">তবে সৌদি আরবের স্বাস্থ্যমন্ত্রী এ ঘটনার জন্য হাজিদের দায়ী করেছেন। তিনি বলেন হাজিরা হজ নিয়ম না মানার কারণেই এ ঘটনা ঘটেছে।</p><p [removed] 0px; margin-bottom: 16px; outline: 0px; overflow: hidden; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">তবে সেখান থেকে বেঁচে যাওয়া এক মিসরীয় বলেন, হাজিদের কোনো দোষ ছিল না। সৌদি আরব হজের জন্য প্রচুর খরচ করে কিন্তু এখানে কোনো সংগঠন নেই। তিনি বলেন, হাজিদের যাওয়ার জন্য একটি রাস্তা ও ফিরে আসার জন্য একটি রাস্তা রাখতে পারত। যদি প্রতিটি রাস্তার মুখে একজন করে পুলিশ থাকত এবং হাজিদের ঠিকভাবে নিয়ন্ত্রণ করত তাহলে এ ধরনের ঘটনা ঘটত না।</p>', 'assets/blog_image/ghatotkacha_by_molee-d6pks0c2.jpg', 'deep kumar das', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `blogger`
--

CREATE TABLE IF NOT EXISTS `blogger` (
  `blogger_id` int(4) NOT NULL,
  `first_name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `email_address` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(32) CHARACTER SET utf8 NOT NULL,
  `mobile_no` varchar(15) CHARACTER SET utf8 NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` varchar(6) CHARACTER SET utf8 NOT NULL,
  `city` varchar(40) CHARACTER SET utf8 NOT NULL,
  `country` varchar(40) CHARACTER SET utf8 NOT NULL,
  `zipcode` varchar(5) CHARACTER SET utf8 NOT NULL,
  `profile_photo` text CHARACTER SET utf8 NOT NULL,
  `blogger_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(3) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `category_description` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `category_description`, `publication_status`) VALUES
(3, 'Local things', 'local things', 0),
(4, 'Delhi', 'Delhi man', 1),
(5, 'man', 'man', 0),
(6, 'woman', 'woman', 0),
(7, 'man', 'man is good', 0),
(8, 'UK', 'United Kingdom', 1),
(9, 'kolkata', 'kolkata is a city', 1),
(10, 'USA', 'United state of  America ', 1),
(11, 'Punjab', 'punjab', 1),
(12, 'Sri lanka', 'Sri lanka', 1),
(13, 'manipur', 'manipur', 1),
(14, 'New Ali Pore', 'New Alipore', 1),
(15, 'Hindustan', 'hindustan', 1),
(16, 'japan', 'japan', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `comments_id` int(9) NOT NULL,
  `blog_id` int(5) NOT NULL,
  `author_name` varchar(50) NOT NULL,
  `author_email` varchar(100) NOT NULL,
  `mobile_no` int(11) NOT NULL,
  `comments` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comments_id`, `blog_id`, `author_name`, `author_email`, `mobile_no`, `comments`, `publication_status`, `created_date_time`) VALUES
(4, 4, 'rhitwick', 'rhitwick@gmail.com', 0, 'welcome\r\n', 0, '2015-10-12 00:41:01'),
(5, 6, 'rhitwick', 'rhitwick@gmail.com', 0, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum.', 1, '2015-10-12 00:53:31'),
(6, 6, 'rhitwick', 'rhitwick@gmail.com', 0, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum. Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu posuere nunc justo tempus leo.', 1, '2015-10-12 01:20:16'),
(7, 6, 'rhitwick', 'rhitwick@gmail.com', 0, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum.', 1, '2015-10-12 01:21:35');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(5) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `email_address`, `password`, `status`) VALUES
(1, 'rhitwick', 'rhitwick@gmail.com', '2138cb5b0302e84382dd9b3677576b24', 0),
(2, 'deep', '4deepkumar@gmail.com', 'b642cdcff9bfe56194063ef1764b652e', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `ajax`
--
ALTER TABLE `ajax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `blogger`
--
ALTER TABLE `blogger`
  ADD PRIMARY KEY (`blogger_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comments_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ajax`
--
ALTER TABLE `ajax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `blog_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `blogger`
--
ALTER TABLE `blogger`
  MODIFY `blogger_id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comments_id` int(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
