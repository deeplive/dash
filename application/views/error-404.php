﻿<!DOCTYPE html>
<!--
Beyond Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.4
Version: 1.4.1
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
    <!--Head-->

    <!-- Mirrored from beyondadmin-v1.4.1.s3-website-us-east-1.amazonaws.com/error-404.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Jul 2015 22:39:25 GMT -->
    <head>
        <meta charset="utf-8" />
        <title>Error 404 - Page Not Found</title>

        <meta name="description" content="Error 404 - Page Not Found" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png" type="image/x-icon">

            <!--Basic Styles-->
            <link href="<?php echo site_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />

            <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" />
            <link href="<?php echo base_url(); ?>assets/css/weather-icons.min.css" rel="stylesheet" />

            <!--Fonts-->
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

                <!--Beyond styles-->
                <link  href="<?php echo base_url(); ?>assets/css/beyond.min.css" rel="stylesheet" />
                <link href="<?php echo base_url(); ?>assets/css/demo.min.css" rel="stylesheet" />
                <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet" />
                <link id="skin-link" href="#" rel="stylesheet" type="text/css" />

                <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
                <script src="<?php echo base_url(); ?>assets/js/skins.min.js"></script>
                </head>
                <!--Head Ends-->
                <!--Body-->
                <body class="body-404">
                    <div class="error-header"> </div>
                    <div class="container ">
                        <section class="error-container text-center">
                            <h1>404</h1>
                            <div class="error-divider">
                                <h2>page not found</h2>
                                <p class="description">We Couldn’t Find This Page</p>
                            </div>
                            <a href="<?php echo base_url(); ?>welcome/index.html" class="return-btn"><i class="fa fa-home"></i>Home</a>
                        </section>
                    </div>
                    <!--Basic Scripts-->
                    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
                    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
                    <script src="<?php echo base_url(); ?>assets/js/slimscroll/jquery.slimscroll.min.js"></script>

                    <!--Beyond Scripts-->
                    <script src="<?php echo base_url(); ?>assets/js/beyond.min.js"></script>

                    <!--Google Analytics::Demo Only-->
                    <script>
                        (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                        })(window, document, 'script', '../www.google-analytics.com/analytics.js', 'ga');

                        ga('create', 'UA-60412744-1', 'auto');
                        ga('send', 'pageview');

                    </script>
                </body>
                <!--Body Ends-->

                <!-- Mirrored from beyondadmin-v1.4.1.s3-website-us-east-1.amazonaws.com/error-404.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Jul 2015 22:39:25 GMT -->
                </html>
