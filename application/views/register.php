﻿<!DOCTYPE html>
<!--
Beyond Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.4
Version: 1.4.1
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
    <!--Head-->

    <!-- Mirrored from beyondadmin-v1.4.1.s3-website-us-east-1.amazonaws.com/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Jul 2015 22:39:25 GMT -->
    <head>
        <meta charset="utf-8" />
        <title>Register Page</title>

        <meta name="description" content="register page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png" type="image/x-icon">

            <!--Basic Styles-->
            <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />
            <link id="bootstrap-rtl-link" href="#" rel="stylesheet" />
            <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" />

            <!--Fonts-->
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

                <!--Beyond styles-->
                <link  href="<?php echo base_url(); ?>assets/css/beyond.min.css" rel="stylesheet" />
                <link href="<?php echo base_url(); ?>assets/css/demo.min.css" rel="stylesheet" />
                <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet" />
                <link id="skin-link" href="#" rel="stylesheet" type="text/css" />

                <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
                <script src="<?php echo base_url(); ?>assets/js/skins.min.js"></script>
                </head>
                <!--Head Ends-->
                <!--Body-->
                <body>
                    <div class="register-container animated fadeInDown">
                        <form action="<?php echo base_url();?>welcome/save_user" method="post">
                        <div class="registerbox bg-white">
                            <div class="registerbox-title">Register</div>

                            <div class="registerbox-caption ">Please fill in your information</div>
                            <div class="registerbox-textbox">
                                <input type="text" class="form-control" name="user_name" placeholder="Username" />
                            </div>
                            <div class="registerbox-textbox">
                                <input type="email" class="form-control" name="email_address" placeholder="Email" />
                            </div>
                            <div class="registerbox-textbox">
                                <input type="password" class="form-control" name="password" placeholder="Enter Password" />
                            </div>
                            
                            <div class="registerbox-submit">
                                <input type="submit" class="btn btn-primary pull-right" value="SUBMIT">
                            </div>
                        </div>
                        </form>
                        <div class="logobox">
                            <div class="text-center text-danger"><h6><b>
                                            <?php
                                            $exe = $this->session->userdata('exception');
                                            if ($exe) {
                                                echo $exe;
                                                $this->session->unset_userdata('exception');
                                            }
                                            ?>
                                        </b></h6></div>

                                <div class="text-center text-success"><h6><b>
                                            <?php
                                            $msg = $this->session->userdata('massage');
                                            if ($msg) {
                                                echo $msg;
                                                $this->session->unset_userdata('massage');
                                            }
                                            ?>
                                            <a href="<?php echo base_url();?>welcome/login">Login</a>
                                        </b>
                                    </h6>
                                </div>
                        </div>
                    </div>

                    <!--Basic Scripts-->
                    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
                    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
                    <script src="<?php echo base_url(); ?>assets/js/slimscroll/jquery.slimscroll.min.js"></script>

                    <!--Beyond Scripts-->
                    <script src="<?php echo base_url(); ?>assets/js/beyond.min.js"></script>
                    <!--Google Analytics::Demo Only-->
                    <script>
                        (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                        })(window, document, 'script', '../www.google-analytics.com/analytics.js', 'ga');

                        ga('create', 'UA-60412744-1', 'auto');
                        ga('send', 'pageview');

                    </script>
                </body>
                <!--Body Ends-->

                <!-- Mirrored from beyondadmin-v1.4.1.s3-website-us-east-1.amazonaws.com/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Jul 2015 22:39:25 GMT -->
                </html>
