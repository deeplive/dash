<h5 class="row-title"><i class="typcn typcn-tag"></i>Profile Photo Upload</h5>
<div class="row">
    <div class="col-xs-1 col-md-1"></div>
    <div class="col-xs-16 col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Upload your Profile Photo</h4>


                <span class="widget-caption text-success">
                    <small><b>
                            <?php
                            $msg = $this->session->userdata('message');
                            if ($msg) {
                                echo $msg;
                                $this->session->unset_userdata('message');
                            }
                            ?>
                    </small></h6>
                </span>
            </div>
            <div class="panel-body">

                <!-- Standar Form -->
                <small>Select files from your computer</small>

                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>super_admin/save_profile_photo" >

                    <div class="form-group">
                        <div class="col-sm-4">
                            <input required="" type="file" name="profile_photo"  >
                        </div>
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">Upload files</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-4"></div>
</div>
<div class="col-xs-1 col-md-1"></div>
</div>