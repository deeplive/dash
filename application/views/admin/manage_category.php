<h5 class="row-title"><i class="typcn typcn-tag"></i>Manage Category</h5>
<div class="row">
    <div class="col-xs-1 col-md-1"></div>
    <div class="col-xs-16 col-md-10">
        <div class="well with-header with-footer">
            <div class="header bordered-pink">
                Manage Category
            </div>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>
                            <i class="fa fa-sort-numeric-asc"></i> Category ID
                        </th>
                        <th class="hidden-xs">
                            <i class="fa fa-user"></i> Category Name
                        </th>
                        <th>
                            <i class="fa fa-sort-alpha-desc"></i> Category Description
                        </th>
                        <th>
                            <i class="fa fa-cog"></i> status
                        </th>
                        <th>
                            <i class="fa fa-cogs"></i> Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($all_category as $v_category) { ?>
                        <tr>
                            <td>
                                <a href="#"><?php echo $v_category->category_id; ?></a>
                            </td>
                            <td class="hidden-xs">
                                <?php echo $v_category->category_name; ?>
                            </td>
                            <td>
                                <?php echo $v_category->category_description; ?>
                            </td>
                            <td>
                                <?php
                                if ($v_category->publication_status == 1) {
                                    ?>
                                    <span href="#" class="label label-success btn-xs "><i class="fa fa-unlock"></i></span>
                                    <?php
                                } else {
                                    ?>
                                    <span href="#" class="label label-danger btn-xs "><i class="fa fa-lock"></i></span> 
                                <?php } ?>
                            </td>
                            <td>
                                <?php
                                if ($v_category->publication_status == 1) {
                                    ?>
                                    <a href="<?php echo base_url(); ?>/super_admin/unpublished_category/<?php echo $v_category->category_id ?>" class="btn btn-danger btn-xs "><i class="fa fa-lock"></i> Unpublished</a>
                                    <?php
                                } else {
                                    ?>
                                    <a href="<?php echo base_url(); ?>/super_admin/published_category/<?php echo $v_category->category_id ?>" class="btn btn-success btn-xs "><i class="fa fa-unlock"></i> Published</a> 
                                <?php } ?>
                                <a href="<?php echo base_url(); ?>/super_admin/edit_category/<?php echo $v_category->category_id ?>" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-edit"></i> Edit</a>

                                <?php
                                $access_label = $this->session->userdata('access_label');
                                if ($access_label == 1) {
                                    ?>
                                    <a href="<?php echo base_url(); ?>/super_admin/delete_category/<?php echo $v_category->category_id ?>" class="btn btn-danger btn-xs black" title="Delete" onclick="return checkDelete();"><i class="fa fa-trash-o"></i> Delete</a>
                                <?php } ?>
                                <a href="<?php echo base_url(); ?>/super_admin/make_pdf/<?php echo $v_category->category_id ?>" class="btn btn-disabled btn-xs blue" title="invoice"><i class="fa fa-share"></i> invoice</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

            <div class="footer">
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                    <nav>
                        <ul class="pagination pagination-sm">
                            <!--                            <li>
                                                            <a href="#" aria-label="Previous">
                                                                <span aria-hidden="true">&laquo;</span>
                                                            </a>
                                                        </li>
                                                        <li class="active"><a href="#">1</a></li>
                                                        <li><a href="#">2</a></li>
                                                        <li><a href="#">3</a></li>
                                                        <li><a href="#">4</a></li>
                                                        <li><a href="#">5</a></li>-->
                            <?php echo $this->pagination->create_links(); ?>
<!--                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>-->
                        </ul>
                    </nav></div>
                <div class="col-sm-4"></div>
            </div>
        </div>
    </div>

    <div class="col-xs-1 col-md-1"></div>
</div>