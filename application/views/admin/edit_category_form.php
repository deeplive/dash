<div class="col-lg-2 col-sm-2 col-xs-3"></div>
<div class="col-lg-8 col-sm-8 col-xs-12">
    <div class="widget">
        <div class="widget-header bordered-bottom bordered-lightred">
            <span class="widget-caption">Edit Category</span>
            <span class="widget-caption text-success">
                <h6><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                        $msg = $this->session->userdata('message');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('message');
                        }
                        ?>
                    </b></h6>
            </span>

        </div>
        <div class="widget-body">
            <div id="horizontal-form">
                <form name="edit_category_form" class="form-horizontal" role="form" action="<?php echo base_url(); ?>super_admin/update_category" method="post">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label no-padding-right">Category Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="category_name" class="form-control" id="inputEmail3" required="" value="<?php echo $category_info->category_name ?>" >
                            <input type="hidden" name="category_id" class="form-control" id="inputEmail3" required="" value="<?php echo $category_info->category_id ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="form-field-8" class="col-sm-3 control-label no-padding-right" >Category Description</label>
                        <div class="col-sm-9">
                            <textarea name="category_description" class="form-control" rows="2"  id="form-field-8" required=""><?php echo $category_info->category_description ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label no-padding-right"></label>
                        <div class="col-sm-9">
                            <div class="control-group">
                                <label>
                                    <input name="publication_status" type="radio" checked="checked" value="1">
                                    <span class="text">Published</span>
                                </label>&nbsp;&nbsp;
                                <label>
                                    <input name="publication_status" type="radio" class="inverted" value="0">
                                    <span class="text">Unpublished</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-10">
                            <button type="submit" class="btn btn-success">Update</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<div class="col-lg-2 col-sm-2 col-xs-3"></div>
<script type="text/javascript">
document.forms['edit_category_form'].elements['publication_status'].value='<?php echo $category_info->publication_status ?>';






</script>