<h5 class="row-title"><i class="typcn typcn-tag"></i>New Blog</h5>
<div class="col-lg-0 col-sm-0 col-xs-0"></div>
<div class="col-lg-12 col-sm-12 col-xs-18">
    <div class="widget">
        <div class="widget-header bordered-bottom bordered-lightred">
            <span class="widget-caption">Add Blog</span>
            <span class="widget-caption text-success">
                <h6><b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                        <?php
                        $msg = $this->session->userdata('message');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('message');
                        }
                        ?>

                    </b></h6>
            </span>

        </div>
        <div class="widget-body">
            <div id="horizontal-form">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>super_admin/save_blog" >
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label no-padding-right">Blog Title</label>
                        <div class="col-sm-8">
                            <input type="text" name="blog_title" class="form-control" id="inputEmail3" required="" placeholder="Category Name" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label no-padding-right">Category Name</label>
                        <div class="col-sm-8">
                            <select id="e2" multiple="multiple" name="category_id" style="width: 100%;">

                                <?php
                                foreach ($all_published_category as $v_category) {
                                    ?>
                                    <option value="<?php echo $v_category->category_id ?>" /><?php echo $v_category->category_name ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="form-field-8" class="col-sm-3 control-label no-padding-right">Blog Short Description</label>
                        <div class="col-sm-8">
                            <div class="widget-body">
                                <div class="widget-main no-padding">
                                    <textarea name="blog_short_description" id="summernote"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="form-field-8" class="col-sm-3 control-label no-padding-right">Blog Long Description</label>
                        <div class="col-sm-8">
                            <div class="widget-body">
                                <div class="widget-main no-padding">
                                    <textarea name="blog_long_description" id="summernote2"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="form-field-8" class="col-sm-3 control-label no-padding-right">Upload Image</label>
                        <div class="col-sm-8">
                            <div class="widget-body">
                                <div class="widget-main no-padding">
                                    <input type="file" name="blog_image">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label no-padding-right"></label>
                        <div class="col-sm-9">
                            <div class="control-group">
                                <label>
                                    <input name="publication_status" type="radio" checked="checked" value="1">
                                    <span class="text">Published</span>
                                </label>&nbsp;&nbsp;
                                <label>
                                    <input name="publication_status" type="radio" class="inverted" value="0">
                                    <span class="text">Unpublished</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-10">
                            <button type="submit" class="btn btn-success">Add Blog</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<div class="col-lg-0 col-sm-0 col-xs-0"></div>