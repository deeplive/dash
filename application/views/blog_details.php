<div class="mainbar">
    <div class="article">
        <span style="font-size: 20px"><?php echo $blog_info->blog_title; ?></span>
        <div class="clr"></div>
        
        <p>Posted by <a href="#"><?php echo $blog_info->author_name; ?></a> <span>&nbsp;&bull;&nbsp;</span> Filed under <a href="#">templates</a>, <a href="#">internet</a></p>
        <?php echo $blog_info->blog_long_description; ?>
        <p>Tagged: <a href="#">orci</a>, <a href="#">lectus</a>, <a href="#">varius</a>, <a href="#">turpis</a></p>
        <p><a href="#"><strong>Comments (3)</strong></a> <span>&nbsp;&bull;&nbsp;</span> May 27, 2010 <span>&nbsp;&bull;&nbsp;</span> <a href="#"><strong>Edit</strong></a></p>
    </div>
    <div class="article">
        <h2><span><?php echo count($comments_by_blog_id); ?></span> Responses</h2>
        <div class="clr"></div>
        <?php
        foreach ($comments_by_blog_id as $v_comments){
        ?>
        <div class="comment"> <a href="#"><img src="<?php echo base_url(); ?>assets/images/userpic.gif" width="40" height="40" alt="" class="userpic" /></a>
            <p><a href="#"><?php echo $v_comments->author_name; ?></a> Says:<br />
                <?php echo $v_comments->created_date_time; ?></p>
            <p><?php echo $v_comments->comments; ?></p>
        </div>
        <?php } ?>
    </div>
    <?php
    $user_id = $this->session->userdata('user_id');
    if ($user_id != NULL) {
        ?>
        <div class="article">
            <h2><span>Leave a</span> Reply</h2>
            
            <div class="clr"></div>
            <form action="<?php echo base_url(); ?>welcome/save_comments" method="post" id="leavereply">
                <ol>
                    <li>
                        <label for="message">Your Message ->>>
                        <?php
                        $exe = $this->session->userdata('exception');
                        if ($exe) {
                            echo $exe;
                            $this->session->unset_userdata('exception');
                        }
                        ?>
                    
            
                        <?php
                        $msg = $this->session->userdata('massage');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('massage');
                        }
                        ?></label>
                        <input type="hidden" name="blog_id" value="<?php echo $blog_info->blog_id; ?>">
                        <textarea id="message" name="comments" rows="8" cols="50"></textarea>
                    </li>
                    <li>
                        <input type="submit" name="btn" id="imageField" src="<?php echo base_url(); ?>assets/images/submit.gif" class="send" />
                        <div class="clr"></div>
                    </li>
                </ol>
            </form>
        </div>
    <?php } ?>
</div>