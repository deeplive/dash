<?php

class Admin_Model extends CI_Model {

    public function admin_login_check_info($admin_email_address, $admin_password) {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('admin_email_address', $admin_email_address);
        $this->db->where('admin_password', md5($admin_password));

        $query_result = $this->db->get();
        //get one row
        $result = $query_result->row();
        //get multipal row
        // $result = $query_result->result();
        return $result;
    }

}
