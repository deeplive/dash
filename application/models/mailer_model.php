<?php

class Mailer_Model extends CI_Model {
    /**
     * sendEmail
     * @author Bhaumesh Inc <deepinc4@gmail.com>
     * @param --- $data - information to place in the mail
     * $templeteName - html templete to use in mail body
     * @return --- none
     * modified by --- Deep kumar das
     * date --- 07-Nov-2015 (mm/dd/yyyy)
     */
    function sendEmail($data , $templeteName) {
//        echo "<pre>";
//        print_r($data);
        
        $this->load->library('email');
        $this->email->set_mailtype('html');
        $this->email->from($data['from_address'],$data['user_name']);
        $this->email->to($data['to_address']);
//        $this->email->cc($data['cc_address']);
        $this->email->subject($data['subject']);
        $body = $this->load->view('mailScripts/' . $templeteName, $data, TRUE);
        echo $body;
        exit;
        $this->email->message($body);
        $this->email->send();
        $this->email->clear();
    }

}

?>