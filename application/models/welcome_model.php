<?php

class Welcome_Model extends CI_Model {

    public function select_all_published_category() {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('publication_status', 1);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function select_all_published_blog() {
        $this->db->select('*');
        $this->db->from('blog');
        $this->db->where('publication_status', 1);
        $this->db->order_by('blog_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function select_blog_info_by_id($blog_id) {
        $this->db->select('*');
        $this->db->from('blog');
        $this->db->where('blog_id', $blog_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
    public function select_blog_info_by_category_id($category_id) {
        $this->db->select('*');
        $this->db->from('blog');
        $this->db->where('category_id', $category_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function select_recent_blog() {
        $sql = "SELECT * FROM blog ORDER BY blog_id DESC LIMIT 0,3";
        $query_result = $this->db->query($sql);
        $result = $query_result->result();
        return $result;
    }
    public function select_popular_blog() {
        $sql = "SELECT * FROM blog ORDER BY hit_count DESC LIMIT 0,3";
        $query_result = $this->db->query($sql);
        $result = $query_result->result();
        return $result;
    }
    public function select_stikey_post() {
        $sql = "SELECT * FROM blog WHERE stikey_post=1";
        $query_result = $this->db->query($sql);
        $result = $query_result->row();
        return $result;
    }
//    public function select_comments_by_blog_id(){
//        $sql="SELECT * FROM blog ORDER BY hit_count DESC LIMIT 0,3";
//        $query_result=  $this->db->query($sql);
//        $result=$query_result->result();
//        return $result;
//    }
    public function update_hit_count($total_hit, $blog_id) {
        $this->db->set('hit_count', $total_hit);
        $this->db->where('blog_id', $blog_id);
        $this->db->update('blog');
    }
    public function save_comments_info($data) {
        $this->db->insert('comments', $data);
    }
    public function save_user_info($data) {
        $this->db->insert('user', $data);
        $blogger_id =  $this->db->insert_id();
        return $blogger_id;
    }
    public function user_login_check_info($email_address,$password) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email_address',$email_address);
        $this->db->where('password',md5($password));

        $query_result = $this->db->get();
        //get one row
        $result = $query_result->row();
        //get multipal row
        // $result = $query_result->result();
        return $result;
    }
    public function select_comments_by_blog_id($blog_id) {
        $this->db->select('*');
        $this->db->from('comments');
        $this->db->where('blog_id',$blog_id);
        $this->db->where('publication_status',1);
        
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    public function update_blogger_status($blogger_id) {
        $this->db->set('status',1);
        $this->db->where('user_id',$blogger_id);
        $this->db->update('user');
    }

}
