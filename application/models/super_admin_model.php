<?php

class Super_Admin_Model extends CI_Model {

    public function save_category_info($data) {
        $this->db->insert('category', $data);
    }

    public function select_all_category($per_page, $offset) {

        if ($offset == NULL) {
            $offset = 0;
        }
        $this->db->select('*');
        $this->db->from('category');
        $this->db->limit($per_page, $offset);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function update_unpublished_category($category_id) {
        $this->db->set('publication_status', 0);
        $this->db->where('category_id', $category_id);
        $this->db->update('category');
    }

    public function update_published_category($category_id) {
        $this->db->set('publication_status', 1);
        $this->db->where('category_id', $category_id);
        $this->db->update('category');
    }

    public function update_delete_category($category_id) {
        $this->db->where('category_id', $category_id);
        $this->db->delete('category');
    }

    public function select_category_info_by_id($category_id) {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('category_id', $category_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function update_category_info($data, $category_id) {
        $this->db->where('category_id', $category_id);
        $this->db->update('category', $data);
    }

    public function save_blog_info($data) {
        $this->db->insert('blog', $data);
    }

    public function save_profile_image_info($data, $admin_id) {
        $this->db->where('admin_id', $admin_id);
        $this->db->update('admin', $data);
    }
    public function select_admin_info_by_id($admin_id) {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('admin_id', $admin_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

}
