<?php

// session_start();
class Super_Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $admin_id = $this->session->userdata('admin_id');
        if ($admin_id == NULL) {
            redirect('admin', 'refresh');
        }
        $this->load->model('Super_Admin_Model', 'sa_model');
    }

    public function index() {
        $data = array();
        $admin_id = $this->session->userdata('admin_id', TRUE);
        $data['admin_info']=$this->super_admin_model->select_admin_info_by_id($admin_id);
        $data['maincontent'] = $this->load->view('admin/dashboard', $data, TRUE);
        $data['title'] = 'Dashboard';
        $this->load->view('admin/admin_master', $data);
    }
    public function lock() {
        $data = array();
        $admin_id = $this->session->userdata('admin_id', TRUE);
        $data['admin_info']=$this->super_admin_model->select_admin_info_by_id($admin_id);
        $this->load->view('admin/lock',$data);
    }

    public function add_category() {
        $data = array();
        $admin_id = $this->session->userdata('admin_id', TRUE);
        $data['admin_info']=$this->super_admin_model->select_admin_info_by_id($admin_id);
        $data['maincontent'] = $this->load->view('admin/add_category_form', '', TRUE);
        $data['title'] = 'Add Category';
        $this->load->view('admin/admin_master', $data);
    }
    public function invoice() {
        $data = array();
        $admin_id = $this->session->userdata('admin_id', TRUE);
        $data['admin_info']=$this->super_admin_model->select_admin_info_by_id($admin_id);
        $data['maincontent'] = $this->load->view('admin/invoice', '', TRUE);
        $data['title'] = 'Invoice';
        $this->load->view('admin/admin_master', $data);
    }
    public function make_pdf($category_id) {
        $data = array();
        $admin_id = $this->session->userdata('admin_id', TRUE);
        $data['admin_info']=$this->super_admin_model->select_admin_info_by_id($admin_id);
        
        
        
        
        $this->load->helper('dompdf');
        $view_file =  $this->load->view('admin/invoice', $data, TRUE);
        $file_name = pdf_create($view_file,'Invoice');
        echo $file_name;
    }
    public function save_category() {
        $data = array();
        $admin_id = $this->session->userdata('admin_id', TRUE);
        $data['admin_info']=$this->super_admin_model->select_admin_info_by_id($admin_id);
        $data['category_name'] = $this->input->post('category_name', TRUE);
        $data['category_description'] = $this->input->post('category_description', TRUE);
        $data['publication_status'] = $this->input->post('publication_status', TRUE);
        $this->sa_model->save_category_info($data);

        $sdata = array();
        $sdata['message'] = 'Save Category Information Successfully';
        $this->session->set_userdata($sdata);

        redirect('super_admin/add_category');
    }

    public function manage_category() {
        /*
         * start Pagination
         */
        $data = array();
        $admin_id = $this->session->userdata('admin_id', TRUE);
        $data['admin_info']=$this->super_admin_model->select_admin_info_by_id($admin_id);
        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'super_admin/manage_category';
        $config['total_rows'] = $this->db->count_all('category');
        $config['per_page'] = 8;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $data['all_category'] = $this->super_admin_model->select_all_category($config['per_page'], $this->uri->segment(3));



        //echo $this->pagination->create_links();

        /*
         * End Pagination
         */
//        $data['all_category'] = $this->super_admin_model->select_all_category();
        $data['maincontent'] = $this->load->view('admin/manage_category', $data, true);
        $data['title'] = 'Manage Category';
        $this->load->view('admin/admin_master', $data);
    }

    public function unpublished_category($category_id) {
        $this->super_admin_model->update_unpublished_category($category_id);
        redirect('super_admin/manage_category');
    }

    public function published_category($category_id) {
        $this->super_admin_model->update_published_category($category_id);
        redirect('super_admin/manage_category');
    }

    public function delete_category($category_id) {
        $this->super_admin_model->update_delete_category($category_id);
        redirect('super_admin/manage_category');
    }

    public function edit_category($category_id) {
        $data = array();
        $admin_id = $this->session->userdata('admin_id', TRUE);
        $data['admin_info']=$this->super_admin_model->select_admin_info_by_id($admin_id);
        $data['category_info'] = $this->super_admin_model->select_category_info_by_id($category_id);
        $data['maincontent'] = $this->load->view('admin/edit_category_form', $data, true);
        $data['title'] = 'Edit Category';
        $this->load->view('admin/admin_master', $data);
    }

    public function add_blog() {
        $data = array();
        $admin_id = $this->session->userdata('admin_id', TRUE);
        $data['admin_info']=$this->super_admin_model->select_admin_info_by_id($admin_id);
        $data['all_published_category'] = $this->welcome_model->select_all_published_category();
        $data['maincontent'] = $this->load->view('admin/add_blog', $data, TRUE);
        $data['title'] = 'New Blog';
        $this->load->view('admin/admin_master', $data);
    }

    public function save_blog() {
        $data = array();
        $data['blog_title'] = $this->input->post('blog_title', TRUE);
        $data['category_id'] = $this->input->post('category_id', TRUE);
        $data['blog_short_description'] = $this->input->post('blog_short_description', TRUE);
        $data['blog_long_description'] = $this->input->post('blog_long_description', TRUE);
        $data['author_name'] = $this->session->userdata('admin_full_name');
        $data['publication_status'] = $this->input->post('publication_status', TRUE);

        /*
         * Start image Uploading
         */

//        echo '<pre>';
//        var_dump($_FILES);
//        exit();
//        echo '<pre>';
        $config['upload_path'] = 'assets/blog_image/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 800;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('blog_image')) {
            $error = $this->upload->display_errors();
//            echo $error;
//            exit();
        } else {
            $fdata = $this->upload->data();
            $data['blog_image'] = $config['upload_path'] . $fdata['file_name'];
        }



        /*
         * End image  Uploading
         */

        $this->super_admin_model->save_blog_info($data);

        $sdata = array();
        $sdata['message'] = 'Save Blog Information Successfully';
        $this->session->set_userdata($sdata);

        redirect('super_admin/add_blog');
    }

    public function update_category() {
        $data = array();
        $category_id = $this->input->post('category_id', TRUE);
        $data['category_name'] = $this->input->post('category_name', TRUE);
        $data['category_description'] = $this->input->post('category_description', TRUE);
        $data['publication_status'] = $this->input->post('publication_status', TRUE);
        $this->super_admin_model->update_category_info($data, $category_id);
        redirect('super_admin/manage_category');
    }

    public function photo_upload() {
        $data = array();
        $admin_id = $this->session->userdata('admin_id', TRUE);
        $data['admin_info']=$this->super_admin_model->select_admin_info_by_id($admin_id);
        $data['maincontent'] = $this->load->view('admin/photo_upload', '', TRUE);
        $data['title'] = 'photo upload';
        $this->load->view('admin/admin_master', $data);
    }

    public function save_profile_photo() {
        $data = array();
        $admin_id = $this->session->userdata('admin_id', TRUE);
        //$data['category_name'] = $this->input->post('category_name', TRUE);
        /*
         * Start image Uploading
         */

//        echo '<pre>';
//        var_dump($_FILES);
//        exit();
//        echo '<pre>';
        $config['upload_path'] = 'assets/profile_image/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 800;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $error = '';
        $fdata = array();
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('profile_photo')) {
            $error = $this->upload->display_errors();
//            echo $error;
//            exit();
        } else {
            $fdata = $this->upload->data();
            $data['profile_photo'] = $config['upload_path'] . $fdata['file_name'];
        }



        /*
         * End image  Uploading
         */

        $this->super_admin_model->save_profile_image_info($data, $admin_id);

        $sdata = array();
        $sdata['message'] = 'New Profile Image Save Successfully';
        $this->session->set_userdata($sdata);

        redirect('super_admin/photo_upload');
    }
//    public function photo_details($admin_id){
//        $data=array();
//        
//    }

    public function logout() {
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('admin_full_name');

        $sdata = array();
        $sdata['massage'] = 'You are Successfully Sign Out !';
        $this->session->set_userdata($sdata);
        redirect('admin');
    }

}
