<?php

// session_start();
class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $admin_id = $this->session->userdata('admin_id');
        if ($admin_id != NULL) {
            redirect('super_admin', 'refresh');
        }
    }

    public function index() {
        $this->load->view('admin/login');
    }

    public function admin_login_check() {

        $admin_email_address = $this->input->post('admin_email_address', TRUE);
        $admin_password = $this->input->post('admin_password', TRUE);
        $this->load->model('admin_model', 'a_model');
        $result = $this->a_model->admin_login_check_info($admin_email_address, $admin_password);

        $sdata = array();
        if ($result) {
            $data = array();
            $sdata['admin_full_name'] = $result->admin_full_name;
            $sdata['admin_email_address'] = $result->admin_email_address;
            $sdata['admin_id'] = $result->admin_id;
            $sdata['access_label'] = $result->access_label;
            $this->session->set_userdata($sdata);
            redirect('super_admin');
//        $data['maincontent']=  $this->load->view('admin/dashboard','',TRUE);
//        $data['title']= 'Dashboard';
//        $this->load->view('admin/admin_master',$data);
        } else {
            $sdata['exception'] = 'Your User ID Or Password Invalide!';
            $this->session->set_userdata($sdata);
            redirect('admin');
        }
    }

}
