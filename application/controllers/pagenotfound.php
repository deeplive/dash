<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pagenotfound extends CI_Controller {

    public function index() {
        redirect('/welcome/pagenotfound');
    }

}
