<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $data = array();
        $data['popular_blog'] = $this->welcome_model->select_popular_blog();
    }

    public function index() {
        $data = array();

        $data['all_published_category'] = $this->welcome_model->select_all_published_category();
        $data['recent_blog'] = $this->welcome_model->select_recent_blog();
        $data['popular_blog'] = $this->welcome_model->select_popular_blog();
        $data['stikey_post'] = $this->welcome_model->select_stikey_post();

        $data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
        $data['maincontent'] = $this->load->view('home_content', $data, TRUE);
        $data['title'] = 'Welcome to Website';
        $data['slider'] = 1;
        $data['menu'] = 1;
        $data['sponsors'] = 1;
        $this->load->view('master', $data);
    }

    public function pagenotfound() {

        $this->load->view('error-404');
    }

    public function support() {
        $data = array();

        $data['maincontent'] = $this->load->view('support_content', '', TRUE);
        $data['title'] = 'support';
        $data['slider'] = '';
        $data['menu'] = '';
        $data['sponsors'] = '';
        $this->load->view('master', $data);
    }

    public function about() {
        $data = array();
        $data['all_published_category'] = $this->welcome_model->select_all_published_category();
        $data['maincontent'] = $this->load->view('about_content', '', TRUE);
        $data['title'] = 'about';
        $data['slider'] = '';
        $data['menu'] = 1;
        $data['sponsors'] = '';
        $this->load->view('master', $data);
    }

    public function blog() {
        $data = array();
        $data['all_published_category'] = $this->welcome_model->select_all_published_category();
        $data['popular_blog'] = $this->welcome_model->select_popular_blog();
        $data['recent_blog'] = $this->welcome_model->select_recent_blog();
        $data['maincontent'] = $this->load->view('blog_content', '', TRUE);
        $data['title'] = 'blog';
        $data['slider'] = '';
        $data['menu'] = '';
        $data['sponsors'] = 1;
        $this->load->view('master', $data);
    }

    public function contact() {
        $data = array();
        $data['maincontent'] = $this->load->view('contact_content', '', TRUE);
        $data['title'] = 'contact';
        $data['slider'] = '';
        $data['menu'] = '';
        $data['sponsors'] = '';
        $this->load->view('master', $data);
    }

    public function blog_details($blog_id) {
        $data = array();
        $data['all_published_category'] = $this->welcome_model->select_all_published_category();
        $data['comments_by_blog_id'] = $this->welcome_model->select_comments_by_blog_id($blog_id);
        $data['recent_blog'] = $this->welcome_model->select_recent_blog();

//        $data['comments_by_blog_id'] = $this->welcome_model->select_comments_by_blog_id();
        $data['blog_info'] = $this->welcome_model->select_blog_info_by_id($blog_id);
        /*   echo '<pre>';
          print_r($data['blog_info']);
          echo $data['blog_info']->hit_count;
          exit(); */
        $total_hit = $data['blog_info']->hit_count + 1;
        $this->welcome_model->update_hit_count($total_hit, $blog_id);
        $data['popular_blog'] = $this->welcome_model->select_popular_blog();
        $data['maincontent'] = $this->load->view('blog_details', $data, TRUE);
        $data['title'] = 'Single Blog';
        $data['slider'] = '';
        $data['menu'] = '';
        $data['sponsors'] = 1;
        $this->load->view('master', $data);
    }

    public function category_blog($category_id) {
        $data = array();
        $data['all_published_category'] = $this->welcome_model->select_all_published_category();
        $data['category_blog'] = $this->welcome_model->select_blog_info_by_category_id($category_id);
        $data['maincontent'] = $this->load->view('category_blog', $data, TRUE);
        $data['title'] = 'Single Blog';
        $data['slider'] = '';
        $data['menu'] = 1;
        $data['sponsors'] = '';
        $this->load->view('master', $data);
    }

    public function save_comments() {
        $data = array();
        $data['blog_id'] = $this->input->post('blog_id', TRUE);
        $data['author_name'] = $this->session->userdata('user_name');
        $data['author_email'] = $this->session->userdata('email_address');
        $data['comments'] = $this->input->post('comments', TRUE);
        $this->welcome_model->save_comments_info($data);
        $sdata = array();
        $sdata['exception'] = 'Your Comments Save Successfully Post';
        $this->session->set_userdata($sdata);
        redirect('welcome/blog_details/' . $data['blog_id']);
    }

    public function register() {
        $this->load->view('register');
    }

    public function login() {
        $this->load->view('login');
    }

    public function save_user() {
        $data = array();
        $data['user_name'] = $this->input->post('user_name', TRUE);
        $data['email_address'] = $this->input->post('email_address', TRUE);
        $data['password'] = md5($this->input->post('password', TRUE));
        $blogger_id = $this->welcome_model->save_user_info($data);

//        echo '<pre>';
//        print_r($_FILES);
//        exit();


        /*
         * Start Activation Email 
         */
        $mdata = array();
        $mdata['from_address'] = 'dash@gmail.com';
        $mdata['admin_full_name'] = 'abc.com';
        $mdata['to_address'] = $data['email_address'];
        $mdata['subject'] = 'Activation Email';
        $mdata['user_name'] = $data['user_name'];
        $mdata['password'] = $this->input->post('password', TRUE);
        $mdata['blogger_id'] = $blogger_id;
        $this->mailer_model->sendEmail($mdata, 'activation_email');


        /*
         * End Activation Email 
         */
        $sdata = array();
        $sdata['exception'] = 'Registration Successfull You May Login Now !';
        $this->session->set_userdata($sdata);
        redirect('welcome/register');
    }

    public function user_login_check() {

        $email_address = $this->input->post('email_address', TRUE);
        $password = $this->input->post('password', TRUE);
        //$this->load->model('welcome_model', 'a_model');
        $result = $this->welcome_model->user_login_check_info($email_address, $password);

        $sdata = array();
        if ($result) {
            $data = array();
            $sdata['user_name'] = $result->user_name;
            $sdata['email_address'] = $result->email_address;
            $sdata['user_id'] = $result->user_id;
            // $sdata['access_label'] = $result->access_label;
            $this->session->set_userdata($sdata);
            redirect('welcome');
//        $data['maincontent']=  $this->load->view('admin/dashboard','',TRUE);
//        $data['title']= 'Dashboard';
//        $this->load->view('admin/admin_master',$data);
        } else {
            $sdata['exception'] = 'Your User ID Or Password Invalide !';
            $this->session->set_userdata($sdata);
            redirect('welcome/login');
        }
    }

    public function logout() {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_name');
        redirect('welcome');
    }

    public function update_blogger_status($en_blogger_id) {
        $blogger_id = $this->encrypt->decode(urldecode($en_blogger_id));
        $this->welcome_model->update_blogger_status($blogger_id);
    }

    public function encode() {
        // encode & decode code
        $id = 10;
        $this->load->library('encrypt');
        $id = urlencode($this->encrypt->encode($id));
        echo '----' . $id . '<br />----';
        $id = $this->encrypt->decode(urldecode($id));
        echo $id;
    }

}
